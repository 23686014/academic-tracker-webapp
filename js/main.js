var pageCounter = 1;
var moduleContainer = document.getElementById('module-info');
var btn = document.getElementById("btn");

btn.addEventListener("click", function(){
  var ourRequest = new XMLHttpRequest();
  ourRequest.open('GET', 'https://raw.githubusercontent.com/profharimohanpandey/CW2/master/module-'+ pageCounter +'.json');
  ourRequest.onload = function(){
    console.log(ourRequest.responseText);
    var ourData = JSON.parse(ourRequest.responseText);
    console.log(ourData[0]);
    renderHTML(ourData);
  };
  ourRequest.send();
pageCounter++;
if (pageCounter > 3){
btn.classList.add("hide-me");
  btn.disabled = true;
}
});

function renderHTML(data){
  var htmlString = "";

  for(i = 0; i < data.length; i++){
    htmlString += "<p>" + data[i].Name + " is a " + data[i].Course + " has assements "; //".</p>";
    for(ii = 0; ii < data[i].Module.Assignment.length; ii++){
      if (ii == 0){
        htmlString += data[i].Module.Assignment[ii];
      } else {
        htmlString += " and " + data[i].Module.Assignment[ii];
      }
    }
    htmlString += ' and Learning Outcome ';
    for(ii = 0; ii < data[i].Module.Learning_outcomes.length; ii++){
      if (ii == 0){
        htmlString += data[i].Module.Learning_outcomes[ii];
      } else {
        htmlString += " and " + data[i].Module.Learning_outcomes[ii];
      }
    }

    htmlString += ' and Volume ';
    for(ii = 0; ii < data[i].Module.Volume.length; ii++){
      if (ii == 0){
        htmlString += data[i].Module.Volume[ii];
      } else {
        htmlString += " and " + data[i].Module.Volume[ii];
      }
    }

    htmlString += ' and weights ';
    for(ii = 0; ii < data[i].Module.weights.length; ii++){
      if (ii == 0){
        htmlString += data[i].Module.weights[ii];
      } else {
        htmlString += " and " + data[i].Module.weights[ii];
      }
    }
    htmlString += '.</p>';
  }
  moduleContainer.insertAdjacentHTML('beforeend', htmlString);

}


// var moduleContainer = document.getElementById('module-info');


// // HTML page sections
// var modulesSection = document.getElementById("modules-section");
// var assessmentsSection = document.getElementById("assessments-section");

// // HTML page buttons
// var modulesButton = document.getElementById("modules-button");
// var assessmentsButton = document.getElementById("assessments-button");

//     // Insert rendered HTML data to the degree programmes list
//     document.getElementById('degrees-list').innerHTML = html;

//     // Hide all pages except for degree programmes
//     modulesSection.hidden = true;
//     // assessmentsSection.hidden = true;
//     // createModuleSection.hidden = true;
//     // createDegreeSection.hidden = true;
//     // createAssessmentSection.hidden = true;
//     // degreesSection.hidden = false;
    
// // Renders the degree programmes page
// // function renderDegreesPage(data, mData, aData);

// // JSON data 
// var degreesData;
// var modulesData;
// var assessmentsData;

// // Request JSON data during page load
// requestDegrees();
// requestModules();
// requestAssessments();

// // Degree Programmes JSON data request
// function requestDegrees()
// {
//   var degreesRequest = new XMLHttpRequest();
//   degreesRequest.open('GET', 'https://bitbucket.org/23700271/cw2-json/raw/275b40d6cf62988a59108c4c7f436c550c49eae6/degree.json');
//   degreesRequest.onload = function()
//   {
//     degreesData = JSON.parse(degreesRequest.responseText);
//   };
//   degreesRequest.send();
// }

// // Modules JSON data request
// function requestModules()
// {
//   var modulesRequest = new XMLHttpRequest();
//   modulesRequest.open('GET', 'https://bitbucket.org/23700271/cw2-json/raw/275b40d6cf62988a59108c4c7f436c550c49eae6/module.json');
  
//   modulesRequest.onload = function()
//   {
//     modulesData = JSON.parse(modulesRequest.responseText);
//   };
//   modulesRequest.send();
// }

// // Assessments JSON data request
// function requestAssessments()
// {
//   var assessmentsRequest = new XMLHttpRequest();
//   assessmentsRequest.open('GET', 'https://bitbucket.org/23700271/cw2-json/raw/275b40d6cf62988a59108c4c7f436c550c49eae6/assessment.json');
//   assessmentsRequest.onload = function()
//   {
//     assessmentsData = JSON.parse(assessmentsRequest.responseText);
//   };
//   assessmentsRequest.send();
// }


/**
 * Event listener for degrees button
 * Calls function to render the degree programmes page
 */

// HTML pages
var dashboard = document.getElementById("dashboard");
var degreePage = document.getElementById("degree-page");
var modulePage = document.getElementById("module-page");
var assessmentPage = document.getElementById("assessment-page");


// HTML page link buttons
var dashboardButton = document.getElementById("dashboard-button")
var degreesButton = document.getElementById("degrees-button");
var moduleButton = document.getElementById("module-button");
var assessmentButton = document.getElementById("assessment-button");


// hide dashbaord and reveal degree path page
degreesButton.addEventListener("click", function(){
  degreePage.hidden = false;
  dashboard.hidden = true;
  
});

// hide dashbaord and reveal modules page
moduleButton.addEventListener("click", function(){
  dashboard.hidden = true;
  assessmentPage.hidden = true;
  modulePage.hidden = false;
});

// hide dashbaord and reveal assessments page
assessmentButton.addEventListener("click", function(){
  dashboard.hidden = true;
  modulePage.hidden = true;
  degreePage.hidden = true;
  assessmentPage.hidden = false;
  
});


dashboardButton.addEventListener("click", function(){
  dashboard.hidden = false;
  degreePage.hidden = true;
  modulePage.hidden = true;
  assessmentPage.hidden = true;
 

});


// show dashboard on load. hide all other pages
degreePage.hidden = true;
modulePage.hidden = true;
assessmentPage.hidden = true;
dashboard.hidden = false;




